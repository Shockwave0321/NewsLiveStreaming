(function () {
	(function (e, t, n) {
		var r, i, s;
		s = "slidesjs";
		i = {
			width: 940,
			height: 528,
			start: 1,
			navigation: {
				active: !0,
				effect: "slide"
			},
			pagination: {
				active: !0,
				effect: "slide"
			},
			play: {
				active: !1,
				effect: "slide",
				interval: 5e3,
				auto: !1,
				swap: !0,
				pauseOnHover: !1,
				restartDelay: 2500
			},
			effect: {
				slide: {
					speed: 500
				},
				fade: {
					speed: 300,
					crossfade: !0
				}
			},
			callback: {
				loaded: function () {},
				start: function () {},
				complete: function () {}
			}
		};
		r = function () {
			function t(t, n) {
				this.element = t;
				this.options = e.extend(!0, {}, i, n);
				this._defaults = i;
				this._name = s;
				this.init()
			}
			return t
		}();
		r.prototype.init = function () {
			var n, r, i, s, o, u, a = this;
			n = e(this.element);
			this.data = e.data(this);
			e.data(this, "animating", !1);
			e.data(this, "total", n.children().not(".slidesjs-navigation", n).length);
			e.data(this, "current", this.options.start - 1);
			e.data(this, "vendorPrefix", this._getVendorPrefix());
			if (typeof TouchEvent != "undefined") {
				e.data(this, "touch", !0);
				this.options.effect.slide.speed = this.options.effect.slide.speed / 2
			}
			n.css({
				overflow: "hidden"
			});
			n.slidesContainer = n.children().not(".slidesjs-navigation", n).wrapAll("<div class='slidesjs-container'>", n).parent().css({
				overflow: "hidden",
				position: "relative"
			});
			e(".slidesjs-container", n).wrapInner("<div class='slidesjs-control'>", n).children();
			e(".slidesjs-control", n).css({
				position: "relative",
				left: 0
			});
			e(".slidesjs-control", n).children().addClass("slidesjs-slide").css({
				position: "absolute",
				top: 0,
				left: 0,
				width: "100%",
				zIndex: 0,
				display: "none",
				webkitBackfaceVisibility: "hidden"
			});
			e.each(e(".slidesjs-control", n).children(), function (t) {
				var n;
				n = e(this);
				return n.attr("slidesjs-index", t)
			});
			if (this.data.touch) {
				e(".slidesjs-control", n).on("touchstart", function (e) {
					return a._touchstart(e)
				});
				e(".slidesjs-control", n).on("touchmove", function (e) {
					return a._touchmove(e)
				});
				e(".slidesjs-control", n).on("touchend", function (e) {
					return a._touchend(e)
				})
			}
			n.fadeIn(0);
			this.update();
			this.data.touch && this._setuptouch();
			e(".slidesjs-control", n).children(":eq(" + this.data.current + ")").eq(0).fadeIn(0, function () {
				return e(this).css({
					zIndex: 10
				})
			});
			if (this.options.navigation.active) {
				o = e("<a>", {
					"class": "slidesjs-previous slidesjs-navigation",
					href: "#",
					title: "Previous",
					text: "Previous"
				}).appendTo(n);
				r = e("<a>", {
					"class": "slidesjs-next slidesjs-navigation",
					href: "#",
					title: "Next",
					text: "Next"
				}).appendTo(n)
			}
			e(".slidesjs-next", n).click(function (e) {
				e.preventDefault();
				a.stop(!0);
				return a.next(a.options.navigation.effect)
			});
			e(".slidesjs-previous", n).click(function (e) {
				e.preventDefault();
				a.stop(!0);
				return a.previous(a.options.navigation.effect)
			});
			if (this.options.play.active) {
				s = e("<a>", {
					"class": "slidesjs-play slidesjs-navigation",
					href: "#",
					title: "Play",
					text: "Play"
				}).appendTo(n);
				u = e("<a>", {
					"class": "slidesjs-stop slidesjs-navigation",
					href: "#",
					title: "Stop",
					text: "Stop"
				}).appendTo(n);
				s.click(function (e) {
					e.preventDefault();
					return a.play(!0)
				});
				u.click(function (e) {
					e.preventDefault();
					return a.stop(!0)
				});
				this.options.play.swap && u.css({
					display: "none"
				})
			}
			if (this.options.pagination.active) {
				i = e("<ul>", {
					"class": "slidesjs-pagination"
				}).appendTo(n);
				e.each(new Array(this.data.total), function (t) {
					var n, r;
					n = e("<li>", {
						"class": "slidesjs-pagination-item"
					}).appendTo(i);
					r = e("<a>", {
						href: "#",
						"data-slidesjs-item": t,
						html: t + 1
					}).appendTo(n);
					return r.click(function (t) {
						t.preventDefault();
						a.stop(!0);
						return a.goto(e(t.currentTarget).attr("data-slidesjs-item") * 1 + 1)
					})
				})
			}
			e(t).bind("resize", function () {
				return a.update()
			});
			this._setActive();
			this.options.play.auto && this.play();
			return this.options.callback.loaded(this.options.start)
		};
		r.prototype._setActive = function (t) {
			var n, r;
			n = e(this.element);
			this.data = e.data(this);
			r = t > -1 ? t : this.data.current;
			e(".active", n).removeClass("active");
			return e(".slidesjs-pagination li:eq(" + r + ") a", n).addClass("active")
		};
		r.prototype.update = function () {
			var t, n, r;
			t = e(this.element);
			this.data = e.data(this);
			e(".slidesjs-control", t).children(":not(:eq(" + this.data.current + "))").css({
				display: "none",
				left: 0,
				zIndex: 0
			});
			r = t.width();
			n = this.options.height / this.options.width * r;
			this.options.width = r;
			this.options.height = n;
			return e(".slidesjs-control, .slidesjs-container", t).css({
				width: r,
				height: n
			})
		};
		r.prototype.next = function (t) {
			var n;
			n = e(this.element);
			this.data = e.data(this);
			e.data(this, "direction", "next");
			t === void 0 && (t = this.options.navigation.effect);
			return t === "fade" ? this._fade() : this._slide()
		};
		r.prototype.previous = function (t) {
			var n;
			n = e(this.element);
			this.data = e.data(this);
			e.data(this, "direction", "previous");
			t === void 0 && (t = this.options.navigation.effect);
			return t === "fade" ? this._fade() : this._slide()
		};
		r.prototype.goto = function (t) {
			var n, r;
			n = e(this.element);
			this.data = e.data(this);
			r === void 0 && (r = this.options.pagination.effect);
			t > this.data.total ? t = this.data.total : t < 1 && (t = 1);
			if (typeof t == "number") return r === "fade" ? this._fade(t) : this._slide(t);
			if (typeof t == "string") {
				if (t === "first") return r === "fade" ? this._fade(0) : this._slide(0);
				if (t === "last") return r === "fade" ? this._fade(this.data.total) : this._slide(this.data.total)
			}
		};
		r.prototype._setuptouch = function () {
			var t, n, r, i;
			t = e(this.element);
			this.data = e.data(this);
			i = e(".slidesjs-control", t);
			n = this.data.current + 1;
			r = this.data.current - 1;
			r < 0 && (r = this.data.total - 1);
			n > this.data.total - 1 && (n = 0);
			i.children(":eq(" + n + ")").css({
				display: "block",
				left: this.options.width
			});
			return i.children(":eq(" + r + ")").css({
				display: "block",
				left: -this.options.width
			})
		};
		r.prototype._touchstart = function (t) {
			var n, r;
			n = e(this.element);
			this.data = e.data(this);
			r = t.originalEvent.touches[0];
			this._setuptouch();
			e.data(this, "touchtimer", Number(new Date));
			e.data(this, "touchstartx", r.pageX);
			e.data(this, "touchstarty", r.pageY);
			return t.stopPropagation()
		};
		r.prototype._touchend = function (t) {
			var n, r, i, s, o, u, a, f = this;
			n = e(this.element);
			this.data = e.data(this);
			u = t.originalEvent.touches[0];
			s = e(".slidesjs-control", n);
			if (s.position().left > this.options.width * .5 || s.position().left > this.options.width * .1 && Number(new Date) - this.data.touchtimer < 250) {
				e.data(this, "direction", "previous");
				this._slide()
			} else if (s.position().left < -(this.options.width * .5) || s.position().left < -(this.options.width * .1) && Number(new Date) - this.data.touchtimer < 250) {
				e.data(this, "direction", "next");
				this._slide()
			} else {
				i = this.data.vendorPrefix;
				a = i + "Transform";
				r = i + "TransitionDuration";
				o = i + "TransitionTimingFunction";
				s[0].style[a] = "translateX(0px)";
				s[0].style[r] = this.options.effect.slide.speed * .85 + "ms"
			}
			s.on("transitionend webkitTransitionEnd oTransitionEnd otransitionend MSTransitionEnd", function () {
				i = f.data.vendorPrefix;
				a = i + "Transform";
				r = i + "TransitionDuration";
				o = i + "TransitionTimingFunction";
				s[0].style[a] = "";
				s[0].style[r] = "";
				return s[0].style[o] = ""
			});
			return t.stopPropagation()
		};
		r.prototype._touchmove = function (t) {
			var n, r, i, s, o;
			n = e(this.element);
			this.data = e.data(this);
			s = t.originalEvent.touches[0];
			r = this.data.vendorPrefix;
			i = e(".slidesjs-control", n);
			o = r + "Transform";
			e.data(this, "scrolling", Math.abs(s.pageX - this.data.touchstartx) < Math.abs(s.pageY - this.data.touchstarty));
			if (!this.data.animating && !this.data.scrolling) {
				t.preventDefault();
				this._setuptouch();
				i[0].style[o] = "translateX(" + (s.pageX - this.data.touchstartx) + "px)"
			}
			return t.stopPropagation()
		};
		r.prototype.play = function (t) {
			var n, r, i, s = this;
			n = e(this.element);
			this.data = e.data(this);
			if (!this.data.playInterval) {
				if (t) {
					r = this.data.current;
					this.data.direction = "next";
					this.options.play.effect === "fade" ? this._fade() : this._slide()
				}
				e.data(this, "playInterval", setInterval(function () {
					r = s.data.current;
					s.data.direction = "next";
					return s.options.play.effect === "fade" ? s._fade() : s._slide()
				}, this.options.play.interval));
				i = e(".slidesjs-container", n);
				if (this.options.play.pauseOnHover) {
					i.unbind();
					i.bind("mouseenter", function () {
						return s.stop()
					});
					i.bind("mouseleave", function () {
						return s.options.play.restartDelay ? e.data(s, "restartDelay", setTimeout(function () {
							return s.play(!0)
						}, s.options.play.restartDelay)) : s.play()
					})
				}
				e.data(this, "playing", !0);
				e(".slidesjs-play", n).addClass("slidesjs-playing");
				if (this.options.play.swap) {
					e(".slidesjs-play", n).hide();
					return e(".slidesjs-stop", n).show()
				}
			}
		};
		r.prototype.stop = function (t) {
			var n;
			n = e(this.element);
			this.data = e.data(this);
			clearInterval(this.data.playInterval);
			this.options.play.pauseOnHover && t && e(".slidesjs-container", n).unbind();
			e.data(this, "playInterval", null);
			e.data(this, "playing", !1);
			e(".slidesjs-play", n).removeClass("slidesjs-playing");
			if (this.options.play.swap) {
				e(".slidesjs-stop", n).hide();
				return e(".slidesjs-play", n).show()
			}
		};
		r.prototype._slide = function (t) {
			var n, r, i, s, o, u, a, f, l, c, h = this;
			n = e(this.element);
			this.data = e.data(this);
			if (!this.data.animating && t !== this.data.current + 1) {
				e.data(this, "animating", !0);
				r = this.data.current;
				if (t > -1) {
					t -= 1;
					c = t > r ? 1 : -1;
					i = t > r ? -this.options.width : this.options.width;
					o = t
				} else {
					c = this.data.direction === "next" ? 1 : -1;
					i = this.data.direction === "next" ? -this.options.width : this.options.width;
					o = r + c
				}
				o === -1 && (o = this.data.total - 1);
				o === this.data.total && (o = 0);
				this._setActive(o);
				a = e(".slidesjs-control", n);
				t > -1 && a.children(":not(:eq(" + r + "))").css({
					display: "none",
					left: 0,
					zIndex: 0
				});
				a.children(":eq(" + o + ")").css({
					display: "block",
					left: c * this.options.width,
					zIndex: 10
				});
				this.options.callback.start(r + 1);
				if (this.data.vendorPrefix) {
					u = this.data.vendorPrefix;
					l = u + "Transform";
					s = u + "TransitionDuration";
					f = u + "TransitionTimingFunction";
					a[0].style[l] = "translateX(" + i + "px)";
					a[0].style[s] = this.options.effect.slide.speed + "ms";
					return a.on("transitionend webkitTransitionEnd oTransitionEnd otransitionend MSTransitionEnd", function () {
						a[0].style[l] = "";
						a[0].style[s] = "";
						a.children(":eq(" + o + ")").css({
							left: 0
						});
						a.children(":eq(" + r + ")").css({
							display: "none",
							left: 0,
							zIndex: 0
						});
						e.data(h, "current", o);
						e.data(h, "animating", !1);
						a.unbind("transitionend webkitTransitionEnd oTransitionEnd otransitionend MSTransitionEnd");
						a.children(":not(:eq(" + o + "))").css({
							display: "none",
							left: 0,
							zIndex: 0
						});
						h.data.touch && h._setuptouch();
						return h.options.callback.complete(o + 1)
					})
				}
				return a.stop().animate({
					left: i
				}, this.options.effect.slide.speed, function () {
					a.css({
						left: 0
					});
					a.children(":eq(" + o + ")").css({
						left: 0
					});
					return a.children(":eq(" + r + ")").css({
						display: "none",
						left: 0,
						zIndex: 0
					}, e.data(h, "current", o), e.data(h, "animating", !1), h.options.callback.complete(o + 1))
				})
			}
		};
		r.prototype._fade = function (t) {
			var n, r, i, s, o, u = this;
			n = e(this.element);
			this.data = e.data(this);
			if (!this.data.animating && t !== this.data.current + 1) {
				e.data(this, "animating", !0);
				r = this.data.current;
				if (t) {
					t -= 1;
					o = t > r ? 1 : -1;
					i = t
				} else {
					o = this.data.direction === "next" ? 1 : -1;
					i = r + o
				}
				i === -1 && (i = this.data.total - 1);
				i === this.data.total && (i = 0);
				this._setActive(i);
				s = e(".slidesjs-control", n);
				s.children(":eq(" + i + ")").css({
					display: "none",
					left: 0,
					zIndex: 10
				});
				this.options.callback.start(r + 1);
				if (this.options.effect.fade.crossfade) {
					s.children(":eq(" + this.data.current + ")").stop().fadeOut(this.options.effect.fade.speed);
					return s.children(":eq(" + i + ")").stop().fadeIn(this.options.effect.fade.speed, function () {
						s.children(":eq(" + i + ")").css({
							zIndex: 0
						});
						e.data(u, "animating", !1);
						e.data(u, "current", i);
						return u.options.callback.complete(i + 1)
					})
				}
				return s.children(":eq(" + r + ")").stop().fadeOut(this.options.effect.fade.speed, function () {
					s.children(":eq(" + i + ")").stop().fadeIn(u.options.effect.fade.speed, function () {
						return s.children(":eq(" + i + ")").css({
							zIndex: 10
						})
					});
					e.data(u, "animating", !1);
					e.data(u, "current", i);
					return u.options.callback.complete(i + 1)
				})
			}
		};
		r.prototype._getVendorPrefix = function () {
			var e, t, r, i, s;
			e = n.body || n.documentElement;
			r = e.style;
			i = "transition";
			s = ["Moz", "Webkit", "Khtml", "O", "ms"];
			i = i.charAt(0).toUpperCase() + i.substr(1);
			t = 0;
			while (t < s.length) {
				if (typeof r[s[t] + i] == "string") return s[t];
				t++
			}
			return !1
		};
		return e.fn[s] = function (t) {
			return this.each(function () {
				if (!e.data(this, "plugin_" + s)) return e.data(this, "plugin_" + s, new r(this, t))
			})
		}
	})(jQuery, window, document)
}).call(this);

/*! Copyright (c) 2011 Piotr Rochala (http://rocha.la)
 * Dual licensed under the MIT (http://www.opensource.org/licenses/mit-license.php)
 * and GPL (http://www.opensource.org/licenses/gpl-license.php) licenses.
 *
 * Version: 1.3.8
 *
 */
(function (e) {
	e.fn.extend({
		slimScroll: function (f) {
			var a = e.extend({
				width: "auto",
				height: "250px",
				size: "7px",
				color: "#000",
				position: "right",
				distance: "1px",
				start: "top",
				opacity: .4,
				alwaysVisible: !1,
				disableFadeOut: !1,
				railVisible: !1,
				railColor: "#333",
				railOpacity: .2,
				railDraggable: !0,
				railClass: "slimScrollRail",
				barClass: "slimScrollBar",
				wrapperClass: "slimScrollDiv",
				allowPageScroll: !1,
				wheelStep: 20,
				touchScrollStep: 200,
				borderRadius: "7px",
				railBorderRadius: "7px"
			}, f);
			this.each(function () {
				function v(d) {
					if (r) {
						d = d || window.event;
						var c = 0;
						d.wheelDelta && (c = -d.wheelDelta / 120);
						d.detail && (c = d.detail / 3);
						e(d.target || d.srcTarget || d.srcElement).closest("." + a.wrapperClass).is(b.parent()) && n(c, !0);
						d.preventDefault && !k && d.preventDefault();
						k || (d.returnValue = !1)
					}
				}

				function n(d, g, e) {
					k = !1;
					var f = b.outerHeight() - c.outerHeight();
					g && (g = parseInt(c.css("top")) + d * parseInt(a.wheelStep) / 100 * c.outerHeight(), g = Math.min(Math.max(g, 0), f), g = 0 < d ? Math.ceil(g) : Math.floor(g), c.css({
						top: g + "px"
					}));
					l = parseInt(c.css("top")) / (b.outerHeight() - c.outerHeight());
					g =
						l * (b[0].scrollHeight - b.outerHeight());
					e && (g = d, d = g / b[0].scrollHeight * b.outerHeight(), d = Math.min(Math.max(d, 0), f), c.css({
						top: d + "px"
					}));
					b.scrollTop(g);
					b.trigger("slimscrolling", ~~g);
					w();
					p()
				}

				function x() {
					u = Math.max(b.outerHeight() / b[0].scrollHeight * b.outerHeight(), 30);
					c.css({
						height: u + "px"
					});
					var a = u == b.outerHeight() ? "none" : "block";
					c.css({
						display: a
					})
				}

				function w() {
					x();
					clearTimeout(B);
					l == ~~l ? (k = a.allowPageScroll, C != l && b.trigger("slimscroll", 0 == ~~l ? "top" : "bottom")) : k = !1;
					C = l;
					u >= b.outerHeight() ? k = !0 : (c.stop(!0, !0).fadeIn("fast"), a.railVisible && m.stop(!0, !0).fadeIn("fast"))
				}

				function p() {
					a.alwaysVisible || (B = setTimeout(function () {
						a.disableFadeOut && r || y || z || (c.fadeOut("slow"), m.fadeOut("slow"))
					}, 1E3))
				}
				var r, y, z, B, A, u, l, C, k = !1,
					b = e(this);
				if (b.parent().hasClass(a.wrapperClass)) {
					var q = b.scrollTop(),
						c = b.siblings("." + a.barClass),
						m = b.siblings("." + a.railClass);
					x();
					if (e.isPlainObject(f)) {
						if ("height" in f && "auto" == f.height) {
							b.parent().css("height", "auto");
							b.css("height", "auto");
							var h = b.parent().parent().height();
							b.parent().css("height",
								h);
							b.css("height", h)
						} else "height" in f && (h = f.height, b.parent().css("height", h), b.css("height", h));
						if ("scrollTo" in f) q = parseInt(a.scrollTo);
						else if ("scrollBy" in f) q += parseInt(a.scrollBy);
						else if ("destroy" in f) {
							c.remove();
							m.remove();
							b.unwrap();
							return
						}
						n(q, !1, !0)
					}
				} else if (!(e.isPlainObject(f) && "destroy" in f)) {
					a.height = "auto" == a.height ? b.parent().height() : a.height;
					q = e("<div></div>").addClass(a.wrapperClass).css({
						position: "relative",
						overflow: "hidden",
						width: a.width,
						height: a.height
					});
					b.css({
						overflow: "hidden",
						width: a.width,
						height: a.height
					});
					var m = e("<div></div>").addClass(a.railClass).css({
							width: a.size,
							height: "100%",
							position: "absolute",
							top: 0,
							display: a.alwaysVisible && a.railVisible ? "block" : "none",
							"border-radius": a.railBorderRadius,
							background: a.railColor,
							opacity: a.railOpacity,
							zIndex: 90
						}),
						c = e("<div></div>").addClass(a.barClass).css({
							background: a.color,
							width: a.size,
							position: "absolute",
							top: 0,
							opacity: a.opacity,
							display: a.alwaysVisible ? "block" : "none",
							"border-radius": a.borderRadius,
							BorderRadius: a.borderRadius,
							MozBorderRadius: a.borderRadius,
							WebkitBorderRadius: a.borderRadius,
							zIndex: 99
						}),
						h = "right" == a.position ? {
							right: a.distance
						} : {
							left: a.distance
						};
					m.css(h);
					c.css(h);
					b.wrap(q);
					b.parent().append(c);
					b.parent().append(m);
					a.railDraggable && c.bind("mousedown", function (a) {
						var b = e(document);
						z = !0;
						t = parseFloat(c.css("top"));
						pageY = a.pageY;
						b.bind("mousemove.slimscroll", function (a) {
							currTop = t + a.pageY - pageY;
							c.css("top", currTop);
							n(0, c.position().top, !1)
						});
						b.bind("mouseup.slimscroll", function (a) {
							z = !1;
							p();
							b.unbind(".slimscroll")
						});
						return !1
					}).bind("selectstart.slimscroll",
						function (a) {
							a.stopPropagation();
							a.preventDefault();
							return !1
						});
					m.hover(function () {
						w()
					}, function () {
						p()
					});
					c.hover(function () {
						y = !0
					}, function () {
						y = !1
					});
					b.hover(function () {
						r = !0;
						w();
						p()
					}, function () {
						r = !1;
						p()
					});
					b.bind("touchstart", function (a, b) {
						a.originalEvent.touches.length && (A = a.originalEvent.touches[0].pageY)
					});
					b.bind("touchmove", function (b) {
						k || b.originalEvent.preventDefault();
						b.originalEvent.touches.length && (n((A - b.originalEvent.touches[0].pageY) / a.touchScrollStep, !0), A = b.originalEvent.touches[0].pageY)
					});
					x();
					"bottom" === a.start ? (c.css({
						top: b.outerHeight() - c.outerHeight()
					}), n(0, !0)) : "top" !== a.start && (n(e(a.start).position().top, null, !0), a.alwaysVisible || c.hide());
					window.addEventListener ? (this.addEventListener("DOMMouseScroll", v, !1), this.addEventListener("mousewheel", v, !1)) : document.attachEvent("onmousewheel", v)
				}
			});
			return this
		}
	});
	e.fn.extend({
		slimscroll: e.fn.slimScroll
	})
})(jQuery);

$(window).load(function () {
	//Animate into player area after loading
	if ($('.v_playingwrap').length > 0) {
		$('html, body').animate({
			scrollTop: $('.v_playingwrap').offset().top
		}, 600);
	}
});

$(function () {
	//Main slide

	$('.v_slide').slidesjs({
		navigation: {
			effect: 'fade'
		},
		pagination: {
			effect: 'fade'
		},
		play: {
			effect: 'fade',
			auto: false,
			pauseOnHover: true,
			interval: 20000
		},
		effect: {
			fade: {
				speed: 300
			}
		}
	});

	//Expand / close tag list

	// $('.v_tag dd a').click(function () {
	// 	$(this).toggleClass('on');
	// 	$(this).attr('title', $(this).hasClass('on') ? '닫기' : '펼침');
	// 	$(this).find('span').text($(this).hasClass('on') ? '닫기' : '펼침');
	// 	$(this).parent().find('p').slideToggle('fast');
	// });
	// $('#country_btn').click();
	// $('#genre_btn').click();

	//댓글폼 열기/닫기
	$('.v_comment label').click(function () {
		// if ($('.v_comment form').data('logon') == false) {
		// 	//var a = confirm("로그인이 필요합니다, 로그인 하시겠습니까?");
		// 	if (confirm('로그인이 필요한 서비스입니다, 로그인하시겠습니까?')) {
		// 		top.window.location.href = '/login';
		// 		return false;
		// 	} else {
		// 		return false;
		// 	}
		// } else {
			$(this).hide();
			$('.textbox').show();
			$('.textbox textarea').focus();
		// }
	});
	$(document).bind('click', function (e) {
		if ($(e.target).closest('.textbox').length == 0) {
			$('.v_comment label').show();
			$('.textbox').hide();
		} else {
			$('.v_comment label').hide();
			$('.textbox').show();
		}
	});

	$('.v_sort a').click(function () {
		var aLink = $(this);
		var v_tit_btn = aLink.parent().parent().parent().next();
		v_tit_btn.data('page', '1');
		v_tit_btn.find('span').html('1');
		$.ajax({
			url: aLink.data('url'),
			type: 'post',
			dataType: "json",
			async: true,
			success: function (data) {
				aLink.parent().parent().find('.on').removeClass();
				aLink.addClass("on");
				if (data.html != null && data.html != '') {
					aLink.parent().parent().parent().parent().next().html(data.html);
					if (data.page < 30) {
						var page = data.page / 6;
						var page_html = '';
						if (data.page % 6 == 0) {
							page_html = '<span>1</span>/' + page;
						} else {
							page_html = '<span>1</span>/' + (parseInt(page) + 1);
						}
						aLink.parent().parent().parent().next().find('strong').html(page_html);
					} else {
						page_html = '<span>1</span>/5';
						aLink.parent().parent().parent().next().find('strong').html(page_html);
					}
				} else {
					aLink.parent().parent().parent().parent().next().html('<li class="noresult">해당 검색결과가 없습니다.</li>');
					aLink.parent().parent().parent().next().find('strong').html('<span>0</span>/0');
				}
			}
		});
	});

	$('.v_tit_btn a.sp.prev').click(function () {
		var v_tit_btn = $(this).parent();
		var prev_page = v_tit_btn.data('page') - 1;
		if (prev_page < 1) {
			console.log('No previous page');
			return false;
		} else if (prev_page > 5) {
			console.log('No next page');
			return false;
		} else {
			var prev_btn = $(this);
			var vSort = prev_btn.parent().prev('.v_sort');
			var url = vSort.find('.on').data('url') + '?page=' + prev_page;
			$.ajax({
				url: url,
				type: 'post',
				dataType: "json",
				async: true,
				success: function (data) {
					if (data.html != null && data.html != '') {
						vSort.parent().next().html(data.html);
						v_tit_btn.data('page', prev_page);
						prev_btn.parent().find('span').html(prev_page);
					} else {
						vSort.parent().parent().parent().parent().next().html('<li class="noresult">해당 검색결과가 없습니다.</li>');
					}
				}
			});
		}
	});

	$('.v_tit_btn a.sp.next').click(function () {
		var v_tit_btn = $(this).parent();
		var next_page = parseInt(v_tit_btn.data('page')) + 1;
		if (next_page < 2) {
			console.log('Donnot have previous page');
			return false;
		} else if (next_page > 5) {
			console.log('Donnot have next page');
			return false;
		} else {
			var next_btn = $(this);
			var vSort = next_btn.parent().prev('.v_sort');
			var url = vSort.find('.on').data('url') + '?page=' + next_page;
			$.ajax({
				url: url,
				type: 'post',
				dataType: "json",
				async: true,
				success: function (data) {
					if (data.html != null && data.html != '') {
						vSort.parent().next().html(data.html);
						v_tit_btn.data('page', next_page);
						next_btn.parent().find('span').html(next_page);
					} else {
						vSort.parent().parent().parent().parent().next().html('<li class="noresult">해당 검색결과가 없습니다.</li>');
					}
				}
			});
		}
	});

	$(".v_chart .fc a").click(function () {
		var aLink = $(this);
		if (aLink.attr('class') != 'on') {
			$.ajax({
				url: $(this).data('url'),
				type: 'get',
				dataType: "json",
				async: true,
				success: function (data) {
					if (data != null && data != '') {
						$(".v_chart .fc a.on").removeAttr('class');
						aLink.attr('class', 'on');
						$(".v_chart ol").html(data);
					}
					return false;
				}
			});
		}
		return false;
	});

	// $("#comment_form").find('.btn_area button').click(function () {
	// 	var form = $("#comment_form");
	// 	$.ajax({
	// 		url: form.data('url'),
	// 		type: 'post',
	// 		dataType: "json",
	// 		async: true,
	// 		data: {
	// 			type: $(this).data('type'),
	// 			id_in_type: $(this).data('tid'),
	// 			comment: $("#comment_text").val()
	// 				// '_token': form.find('input').val()
	// 		},
	// 		success: function (data) {
	// 			if (data != null && data != '' && data != 'error') {
	// 				$('.comment_list').prepend(data);
	// 				var comment_count = parseInt($(".count").html()) + 1;
	// 				$(".count").html(comment_count);
	// 				$("#comment_text").val('');
	// 			}
	// 			return false;
	// 		}
	// 	});
	// 	return false;
	// });

	$(".v_comment .btn_more").click(function () {
		var btn_more = $(this);
		var pagenum = btn_more.data('pagenum') + 1;
		$.ajax({
			type: 'get',
			url: $(this).data('url') + "?page=" + pagenum,
			success: function (datas) {
				if (datas != '[]' && datas != '' && datas != 'error') {
					btn_more.data('pagenum', pagenum);
					for (var index in datas) {
						$('.comment_list').append(JSON.parse(datas)[index]);
					}
				} else {
					btn_more.text('최종 결과 입니다.');
				}
			}
		});
	});

	$('.spi a.like').click(function () {
		var aBtn = $(this);
		if (aBtn.data('auth')) {
			var type = aBtn.data('type');
			var eid = aBtn.data('eid');
			var tk = aBtn.data('token');
			var myMothod = 'PUT';
			if (type == 'unlike') {
				myMothod = 'DELETE';
			}
			$.ajax({
				type: 'post',
				url: '/favourite/video/' + eid,
				async: true,
				dataType: "json",
				data: {
					_method: myMothod,
					video: eid,
					_token: tk,
				},
				success: function (datas) {
					if (datas.status == 200) {
						var countSpan = parseInt(aBtn.find('span').html());
						if (type == 'like') {
							aBtn.data('type', 'unlike');
							aBtn.addClass('on');
							aBtn.find('span').html(countSpan + 1);
						} else {
							aBtn.data('type', 'like');
							aBtn.removeClass('on');
							aBtn.find('span').html(countSpan - 1);
						}
					} else {
						alert(datas.msg);
					}
				}
			});
		} else {
			if (confirm('해당 서비스는 로그인이 필요합니다.')) {
				window.location.href = '/login';
			} else {
				return false;
			}
		}
	});

	$('.director_name').click(function () {
		window.location.href = $(this).parent().parent().data('action') + '/' + encodeURIComponent(encodeURIComponent($(this).html()));
	});

	$('.cast_name').click(function () {
		window.location.href = $(this).parent().parent().data('action') + '/' + encodeURIComponent(encodeURIComponent($(this).html()));
	});

	$('.v_playlist ol').slimScroll({
		height: '520px',
		wheelStep: 5
	});

	$('button.youku, button.yookoo, button.iqiyi, button.ihd, button.qq, button.qhd, button.daum, button.hls, button.vip').click(function () {
		//alert('here');
		if ($(this).attr('class') == 'vip') {
			realtime();
		}

		$('.v_playlist li').removeClass();
		$('.v_playlist li p button').removeClass('on');
		$(this).addClass('on');
		$(this).parent().parent().addClass($(this).attr('class'));

		var play_id = $(this).data('id');
		var ifr = $('.v_player iframe');

		if ($(this).attr('class').indexOf('youku') != -1) ifr.attr('src',  play_id);
		else if ($(this).attr('class').indexOf('yookoo') != -1) ifr.attr('src', play_id);
		else if ($(this).attr('class').indexOf('iqiyi') != -1) ifr.attr('src', play_id);
		else if ($(this).attr('class').indexOf('ihd') != -1) ifr.attr('src',  play_id);
		else if ($(this).attr('class').indexOf('qq') != -1) ifr.attr('src', play_id);
		else if ($(this).attr('class').indexOf('qhd') != -1) ifr.attr('src', play_id);
		else if ($(this).attr('class').indexOf('daum') != -1) ifr.attr('src', play_id);
		else if ($(this).attr('class').indexOf('hls') != -1) ifr.attr('src', play_id);
		else if ($(this).attr('class').indexOf('vip') != -1) {
			isLogon = $(this).data('logon');
			myToken = $(this).data('token');
			if (isLogon) {
				$.ajax({
					type: 'post',
					url: '/thirdParties/getVC',
					async: true,
					dataType: 'json',
					data: {
						'_token': myToken,
						'play_id': play_id
					},
					success: function (datas) {
						if (datas.status == 500) {
							console.log(data.msg);
							return false;
						} else if (datas.status == 404) {
							if (confirm(datas.msg)) {
								window.location.href = 'https://user.ggmee.com/profile/vip';
							}
							return false;
						} else if (datas.status == 405) {
							alert(datas.msg);
							return false;
						} else {
							ifr.attr('src', 'http://play.m3u8.co/d.php?vip=' + datas.code);
						}
					}
				});
			}
		} else ifr.attr('src', play_id);

		ifr.load(function () {

			var vPlayer = jwplayer();

			vPlayer.on('adBlock');

			var vPlayer = $('.v_player');
			var url = '/vHit/' + vPlayer.data('vid') + '/' + vPlayer.data('total') + '/' + vPlayer.data('week') + '/' + vPlayer.data('month');
			$.ajax({
				url: url,
				type: 'get',
				dataType: 'json',
				cache: false,
				async: true,
				success: function (data) {
					console.log(data.html);
					$('.hit_info').html(data.html);
				}
			});
		});
	});

	//Theater mode
	$('.btn_wide').click(function () {
		if ($('.v_playingwrap').outerHeight() != '761') {
			$('html, body').animate({
				scrollTop: 0
			}, 600);

			$('.k_599x125').slideUp('fast');

			$('.v_playingwrap').addClass('wide').animate({
				height: 761
			}, 'fast');
			$('.v_player').animate({
				width: 1210,
				height: 761,
				zIndex: 50
			}, 'fast');
			$('.v_player iframe').animate({
				width: 1210,
				height: 721
			}, 'fast');

			$('.v_playlist').css({
				overflow: 'hidden',
				height: 761,
				borderWidth: 0
			});
			$('.v_playlist').animate({
				opacity: '.1'
			}, 'fast');
			$('.v_playguide').css('padding', 0);
			$('.v_playguide .help_ly').css('right', 0);

			$(this).addClass('on');
		} else {
			$('html, body').animate({
				scrollTop: $('.v_playingwrap').offset().top
			}, 600);

			$('.k_599x125').slideDown('fast');

			$('.v_playingwrap').removeClass('wide').animate({
				height: 861
			}, 'fast');
			$('.v_player').animate({
				width: 854,
				height: 561
			}, 'fast');
			$('.v_player iframe').animate({
				width: 854,
				height: 521
			}, 'fast');

			$('.v_playlist').css({
				overflow: 'hidden',
				height: 561,
				borderWidth: '1px'
			});
			$('.v_playlist').animate({
				width: 355,
				opacity: 1
			}, 'fast');
			$('.v_playguide').css('padding', '0 10px');
			$('.v_playguide .help_ly').css('right', '10px');

			$(this).removeClass('on');
		}
	});

	//Troubleshoot playback issues
	$('.v_playerguide a').click(function () {
		$('.v_playerhelp').fadeIn('fast');
		return false;
	});
	$('.v_playerhelp a').click(function () {
		$(this).parent().fadeOut('fast');
		return false;
	});

	//Video autoplay
	$('.v_playlist li:eq(0), .v_playlist li:eq(0) p button:eq(0)').addClass('on');
	$('.v_playlist li:eq(0) p button:eq(0)').trigger('click');
});

var vip_button = '';

function realtime() {
	$.ajax({
		type: 'get',
		url: '/thirdParties/realtime',
		async: true,
		dataType: 'json',
		success: function (datas) {
			if (datas.status == 2) {
				alert('로그인 정보가 잘못되었습니다,\n다시 로그인 해주세요.');
				window.location.href = '/login';
			} else if (datas.status == 1) {
				setTimeout("realtime()", 5 * 60 * 1000);
			} else if (datas.status == 0) {
				alert('다른 사용자가 현재 아이디로 등록하였습니다,\n안전을 위하여 재등록하고 비밀번호를 변경하세요.');
				window.location.href = '/login?crturl=' + encodeURIComponent(encodeURIComponent(window.location.href));
			} else {
				if (confirm('로그인이 필요한 서비스입니다,\n로그인 하시겠습니까?')) {
					window.location.href = '/login?crturl=' + encodeURIComponent(encodeURIComponent(window.location.href));
				} else {
					location.reload();
				}
			}
		}
	});
}


// capture click events
// window.onclick = function(e) {
//   var node = e.target;
//   while (node != undefined && node.localName != 'a') {
//     node = node.parentNode;
//   }
//   if (node != undefined) {
//     //alert('Link!: ' + node.href);
//     /* Your link handler here */
//     //return false;  // stop handling the click
//
// 		$.ajax({
// 			type: 'POST',
// 				url: '/countclicks',
// 					data: {
// 						videoId: $('#comment_text').val()
// 				}
// 			})
//
//   } else {
//     //alert('This is not a link: ' + e.target.innerHTML)
//     return true;  // handle other clicks
//   }
// }
