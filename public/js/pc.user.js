$(function () {
	/**
	 * 로그인 폼체크
	 */
	var user_id = $('#user_phone');
	var user_pw = $('#user_pw');
	var user_ct = $('#user_capt');

	var reg_form = $('.reg_form');

	var countdown = 60;

	if ($('.alert').length) {
		setTimeout(function () {
			$('.alert').find('span').remove();
		}, 3000);
	}

	reg_form.find('.btn_code').click(function () {
		var error = false;
		var userVal = user_id.val();
		var captVal = user_ct.val();
		var isPhone = /^(((13[0-9]{1})|(15[0-3]{1})|(15[5-9]{1})|(18[0-9]{1}))+\d{8})$/; //폰번호 정규식
		var isCaptcha = /^[0-9a-zA-Z]+$/;
		var url = '/thirdParties/';
		if (reg_form.data('type') == 'Y') {
			url += 'mobileForgot';
		} else {
			url += 'mobileRegister';
		}
		//아이디 공백체크
		if (is_space(userVal)) {
			alertInUserPage(user_id, '휴대폰번호를 입력해주세요');
			error = true;
		} else if (is_num(userVal)) {
			if (!(isPhone.test(userVal))) {
				alertInUserPage(user_id, '휴대폰번호를 입력해주세요');
				error = true;
			}
		}
		if (!is_space(captVal)) {
			if (!is_num(captVal)) {
				if (!(isCaptcha.test(captVal))) {
					alertInUserPage(user_ct, '인증코드를 입력해주세요');
					error = true;
				}
			}
		} else {
			alertInUserPage(user_ct, '인증코드를 입력해주세요');
			error = true;
		}
		if (error) {
			return false;
		}

		$.ajax({
			url: url,
			type: 'post',
			dataType: "json",
			async: true,
			data: {
				mobile: userVal,
				captcha: user_ct.val(),
				_token: $('.reg_form').find("input[name='_token']").val()
			},
			success: function (data) {
				if (data.error != null) {
					alertInUserPage(user_id, data.error);
					user_ct.val('');
					refreshCaptcha($("#captchaImg"));
					return false;
				}

				if (data.status == 'ok') {
					var position = $(".btn_code").parent();
					position.find('.btn_code').attr('style','display:none');
					position.append("<a href='javascript:void(0);' class='wating_span'>60초후 재발송</a>");
					$(".reg_form").find('button').removeClass();

					countDowning(position, countdown);
					var p_code = $("#user_pcode");
                    p_code.removeAttr('readonly');
                    p_code.focus();
				} else {
					alert(data.msg);
				}
			},
			error: function (xhr, status, data) {
				if (xhr.status == 422)  // Laravel validation failed
				{
					var jsonErrors = xhr.responseJSON;
					user_ct.val('');
					refreshCaptcha($("#captchaImg"));
					$.each(jsonErrors, function (key, value) {
						if (key == 'captcha') {
							alertInUserPage(user_ct, value);
							user_ct.focus();
						} else if (key == 'mobile') {
							alertInUserPage(user_id, value);
							user_id.focus();
						} else {
							alertInUserPage(user_id, '불명의 오류가 생겼습니다. 관리원한테 연락주십시오.');
							user_id.focus();
						}
					});
					return false;
				}

				return false;
			}
		});
	});


	/* 비밀번호 변경폼 */
	$("#security_form").submit(function () {
		var old = $("#old_password");
		var pass = $("#user_pass");
		var confirmed = $("#user_pass2");
		var error = false;
		if (is_space(old.val())) {
			alertInUserPage(old, '현재 비밀번호를 입력해주십시오.');
			error = true;
		}

		if (pass.val().length < 6 || pass.val().length > 20) {
			alertInUserPage(pass, '비밀번호는 최소 6자 최고 20자 입니다.');
			error = true;
		}

		if (confirmed.val().length < 6 || confirmed.val().length > 20) {
			alertInUserPage(confirmed, '비밀번호는 최소 6자 최고 20자 입니다.');
			error = true;
		}

		if (pass.val() != confirmed.val()) {
			alertInUserPage(pass, '두번 입력한 비밀번호가 틀립니다.');
			error = true;
		}

		if (confirm('비밀번호를 성공적으로 변경하였을시 다시 로그인 하여야 합니다.')) {
			return !error;
		} else {
			return false;
		}
	});

	$('.login_form').submit(function () {
		var error = false;
		var userVal = user_id.val();
		var passVal = user_pw.val();
		var captVal = user_ct.val();
		var isEmail = /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/; //이메일 정규식
		// var isPhone = /^(((13[0-9]{1})|(14[0-9]{1})|(17[0]{1})|(15[0-3]{1})|(15[5-9]{1})|(18[0-9]{1}))+\d{8})$/; //폰번호 정규식
		var isPhone = /^(((13[0-9]{1})|(15[0-3]{1})|(15[5-9]{1})|(18[0-9]{1}))+\d{8})$/; //폰번호 정규식
		var isCaptcha = /^[0-9a-zA-Z]+$/;

		//아이디 공백체크
		if (is_space(userVal)) {
			alertInUserPage(user_id, '이메일주소/휴대폰번호를 입력해주세요');
			error = true;
		}
		//비밀번호 공백체크
		if (passVal == '') {
			alertInUserPage(user_id, '비밀번호를 입력해주세요');
			error = true;
		}
		//캅챠 공백체크
		if (is_space(captVal)) {
			alertInUserPage(user_ct, '인증코드를 입력해주세요');
			error = true;
		}
		//이메일격식 체크
		if (!is_num(userVal)) {
			if (!(isEmail.test(userVal))) {
				alertInUserPage(user_id, '이메일주소/휴대폰번호를 입력해주세요');
				error = true;
			}
		}
		//폰번호격식 체크
		if (is_num(userVal)) {
			if (!(isPhone.test(userVal))) {
				alertInUserPage(user_id, '이메일주소/휴대폰번호를 입력해주세요');
				error = true;
			}
		}
		//캅챠격식 체크
		if (!is_num(captVal)) {
			if (!(isCaptcha.test(captVal))) {
				alertInUserPage(user_ct, '인증코드를 입력해주세요');
				error = true;
			}
		}

		return !error;
	});

	/**
	 * 가입폼 체크
	 */
	var u_pcode = $('#user_pcode');

	reg_form.submit(function () {
		var error = false;
		var phoneVal = user_id.val();
		var captVal = user_ct.val();
		var pcodeVal = u_pcode.val();
		var isPhone = /^(((13[0-9]{1})|(15[0-3]{1})|(15[5-9]{1})|(18[0-9]{1}))+\d{8})$/; //폰번호 정규식
		var user_pw2 = $('#user_pw2');

		if ($('.reg_form').find('.gray').attr('class') == 'gray') {
			alertInUserPage(u_pcode, '휴대폰 인증번호를 받아주세요');
			return false;
		}

		//폰번호 공백체크
		if (is_space(phoneVal)) {
			alertInUserPage(user_id, '휴대폰번호를 정확히 입력해주세요');
			error = true;
		}
		//폰번호격식 체크
		if (is_num(phoneVal)) {
			if (!(isPhone.test(phoneVal))) {
				alertInUserPage(user_id, '휴대폰번호를 정확히 입력해주세요2');
				error = true;
			}
		}
		//캅챠 공백체크
		// if (is_space(captVal) || captVal.length != 5) {
		//     user_ct.parent().addClass('alert');
		//     user_ct.parent().append('<span>정확한 인증코드를 입력해주세요</span>');
		//     setTimeout(function () {
		//         user_ct.parent().find('span').remove();
		//         user_ct.focus();
		//     }, 20000);
		//     error = true;
		// }
		//폰인증번호 공백체크
		if (is_space(pcodeVal) || pcodeVal.length != 6) {
			alertInUserPage(u_pcode, '폰인증번호를 정확히 입력해주세요');
			error = true;
		}

		return !error;
	});

	/* 비번바꾸기 체크*/
	$('.chgPwd_form').submit(function () {
		var error = false;
		var user_pass = $("#user_pass");
		var user_pass2 = $("#user_pass2");
		if (is_space(user_pass.val())) {
			alertInUserPage(user_pass, '비밀번호를 입력해주세요');
			error = true;
		}
		if (is_space(user_pass2.val())) {
			alertInUserPage(user_pass2, '비밀번호를 다시 입력해주세요');
			error = true;
		}

		if (user_pass.val().length < 6 || user_pass.val().length > 20) {
			alertInUserPage(user_pass, '비밀번호는 최소6자 최고20자가 가능합니다.');
			error = true;
		}

		if (user_pass.val() != user_pass2.val()) {
			alertInUserPage(user_pass2, '두번입력한 비밀번호가 다릅니다.');
			error = true;
		}

		return !error;
	});

	//입력할땐 alert클래스를 지운다
	$('#user_phone, #user_ct, #user_pcode, #user_pass, #user_pass2, #user_nick, #old_password, #userNick, #realName').bind('input propertychange', function () {
		$(this).parent().removeClass('alert');
	});

	//캡챠를 다시 불러오기 액션처리
	$("#captchaImg").click(function () {
		refreshCaptcha($(this));
	});

	var identity_form = $("#identity_form");
	var code_btn = identity_form.find(".btn_code");
	code_btn.click(function () {
		if(is_space(user_id.val())){
			alert('전화번호를 입력해 주십시오.');
			return false;
		}
		if(is_space(user_ct.val())){
			alert('이미지 인증코드를 입력해 주십시오.');
			return false;
		}
		var isPhone = /^(((13[0-9]{1})|(15[0-3]{1})|(15[5-9]{1})|(18[0-9]{1}))+\d{8})$/;
		var isCaptcha = /^[0-9a-zA-Z]+$/;
		if (!isPhone.test(user_id.val())) {
			alert('정확한 전화번호를 입력해 주십시오.');
			return false;
		}
		if (!isCaptcha.test(user_ct.val())) {
			alert('정확한 이미지 인증코드를 입력해 주십시오.');
			return false;
		}

		$.ajax({
			url: '/addMobile',
			type: 'post',
			dataType: "json",
			async: true,
			data: {
				mobile: user_id.val(),
				captcha: user_ct.val(),
				_token: identity_form.data('token')
			},
			success: function (data) {
				if (data.error != null) {
					alertInUserPage(user_id, data.error);
					user_ct.val('');
					refreshCaptcha($("#captchaImg"));
					return false;
				}

				if (data.status == 'ok') {
					code_btn.attr('style','display:none');
					var waiting ="<a href='javascript:void(0);' class='wating_span'>60초후 재발송</a>";
                    code_btn.parent().append(waiting);
                    identity_form.find('button').removeClass();

					countDowning(code_btn.parent(), countdown);

					var user_code = $("#user_code");
                    user_code.removeAttr('readonly');
                    user_code.focus();

                    identity_form.find('button').removeAttr('class');
                    user_ct.parent().attr('class','');
                    user_id.parent().attr('class','');
				} else {
					alert(data.msg);
				}
			},
			error: function (xhr, status, data) {
				if (xhr.status == 422)  // Laravel validation failed
				{
					var jsonErrors = xhr.responseJSON;
					user_ct.val('');
					refreshCaptcha($("#captchaImg"));
					$.each(jsonErrors, function (key, value) {
						if (key == 'captcha') {
							// alertInProfilePage(user_ct, value);
                            alertInProfilePage(user_ct, value);
							user_ct.focus();
						} else if (key == 'mobile') {
							// alertInProfilePage(user_id, value);
                            alertInProfilePage(user_id, value);
							user_id.focus();
						} else {
							// alertInProfilePage(user_id, '불명의 오류가 생겼습니다. 관리원한테 연락주십시오.');
                            alertInProfilePage(user_id, '불명의 오류가 생겼습니다. 관리원한테 연락주십시오.');
							user_id.focus();
						}
					});
					return false;
				}

				return false;
			}
		});
	});

	var identity_submit = identity_form.find('button');
	identity_submit.click(function () {
		if (is_space(user_id.val())) {
			alert('전화번호를 입력해 주십시오.');
			return false;
		}
		var isPhone = /^(((13[0-9]{1})|(15[0-3]{1})|(15[5-9]{1})|(18[0-9]{1}))+\d{8})$/;
		var isCode = /^[0-9]{6}$/;

		if (!isPhone.test(user_id.val())) {
			alert('정확한 전화번호를 입력해 주십시오.');
			return false;
		}
		var user_code = $("#user_code");
		if(is_space(user_code.val())){
			alert('모바일 인증번호를 입력해 주십시오.');
			return false;
		}
		if (!isCode.test(user_code.val())) {
			alert('정확한 모바일 인증번호를 입력해 주십시오.');
			return false;
		}
		if (identity_submit.find(".gray").length==0) {
			identity_submit.parent().parent().submit();
			return true;
		} else {
			alert('모바일 인증번호를 받으십시오.');
			return false;
		}
	});

	//결제리다이렉트
	var paySecond = $('.vip_payredirect em');
	var payWait = paySecond.html();
	payRedirect();

	function payRedirect() {
		if (payWait != 0) {
			setTimeout(function() {
				paySecond.text(--payWait);
				payRedirect();
			}, 1000);
		}
		else {
			$('.pay_redirect_form').submit();
		}
	}

	if($('.user_content table').length>0){
		getPurchases(1,$('.user_content').find('table').data('url'));
	}

    $(".vip_item .fc li a").click(function () {
        var pay = $(this).data('paytype');
        if(pay!='wechat'&&pay!='alipay'){
            return false;
        }
        var item = $(this).parent().data('id');
        var form = $(".vip_item").find('form');

        $.ajax({
            url: "/checkMLogin",
            type: "post",
            async: true,
            success: function (datas) {
                var data = JSON.parse(datas);
				if(data.status == 200){
					form.find('input[name="item"]').val(item);
					form.find('input[name="pay_type"]').val(pay);
					form.submit();
                }else if(data.status == 422){
					window.location.href = '/login';
				}else if(data.status == 201){
					var msg = '휴대폰에서도 사용할 수 있기위해 휴대폰 인증이나 QQ계정연결이 필요합니다. 지금 바로 설정하시겠습니까?';
					if(confirm(msg)){
                        window.location.href = 'http://user.ggmee.com/profile/security';
					}else{
                        form.find('input[name="item"]').val(item);
                        form.find('input[name="pay_type"]').val(pay);
                        form.submit();
					}
				}else{
					return false;
				}
            }
        });


    });

	// $(".user_service.fc li").click(function () {
	// 	window.open($(".user_service.fc").data('url'));
	// });
});

//캡챠를 다시 불러오기
function refreshCaptcha(captcha_img_div_id) {
	var url = "/captcha/" + Math.random();
	captcha_img_div_id.attr('src', url); // captcha_img_div_id = 'captchaImg'
}

function countDowning(position, countdown) {
	if (countdown == 0) {
		$(".wating_span").remove();
		position.attr('style','');
		$("#captchaImg").click();
		var user_ct =$('#user_capt');
        user_ct.val('');
        user_ct.focus();
        if(position.find('.btn_code').attr('style')!=null){
            position.find('.btn_code').attr('style','');
		}
		return;
	} else {
		$(".wating_span").html(countdown + '초후 재발송');
		countdown--;
	}
	setTimeout(function () {
            countDowning(position, countdown);
		}
		, 1000);
}

function alertInUserPage(domObject, message) {
	domObject.parent().addClass('alert');
	domObject.parent().append('<span>' + message + '</span>');
	setTimeout(function () {
		domObject.parent().find('span').remove();
	}, 2000);
}
function alertInProfilePage(domObject, message) {
	domObject.parent().addClass('alert1');
	domObject.parent().find('img').attr('style','display:none;');
	domObject.parent().append('<span class="alert_msg">' + message + '</span>');
	setTimeout(function () {
		domObject.parent().find('.alert_msg').remove();
		domObject.parent().find('img').attr('style','');
	}, 2000);
}

function getPurchases($page,$url) {
	$.ajax({
		url: $url+"?page=" + $page,
		type: "get",
		async: true,
		success: function (data) {
			var datas = JSON.parse(data);
			if(datas.status == 200){
				$('tbody').html(datas.html);
				$('.user_content .user_paging').html(datas.aLinks);
			}
		}
	});
}