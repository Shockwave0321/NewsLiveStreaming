$(function(){
	$('.btn_faq').on('click', function(e) {
		var _this = $(this);
		_this.closest('li').addClass('on');
		$('.faq_list>li').each(function(index){
			if (index != _this.closest('li').index()) {
				$(this).removeClass('on');
			}
		});

		// Smooth movement
		$('html, body').animate({
			scrollTop : _this.closest('li').offset().top
		}, 300);
	});

	var showFaqId = 0;
	if (showFaqId > 0) {
		$('#' + showFaqId).find('.btn_faq').trigger('click');
	}
	else {
		var _href = window.location.href;
		if (_href.indexOf('#') > -1) {
			var anchorFaqId = _href.substring(_href.indexOf('#') + 1);
			if ($('#' + anchorFaqId).length > 0) {
				$('#' + anchorFaqId).find('.btn_faq').trigger('click');
			}
			else {
				window.location.href = '#';
			}
		}
	}
});