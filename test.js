const Nightmare = require('nightmare')

let test = (url) => {
  return new Promise(async (resolve, reject) => {
    let nightmare = new Nightmare({
      show: false,
      waitTimeout: 5000
    })
    let error = false

    await nightmare.goto(url).wait('h1').evaluate(() => {
      let h1 = document.querySelector('h1')
      return h1.innerHTML
    }).then((h1) => {
      resolve(h1)
      return nightmare.end()
    }).catch(err => {
      error = true
      return nightmare.end()
    })

    if (error) {
      test(url)
    }
  })
}

test('http://example.com').then(data => {
  console.log(data)
})
