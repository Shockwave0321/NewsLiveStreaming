module.exports = (sequelize, Sequelize) => {
    var User = sequelize.define('user', {
        id: { autoIncrement: true, primaryKey: true, type: Sequelize.INTEGER },
        firstname: { type: Sequelize.STRING, notEmpty: true },
        lastname: { type: Sequelize.STRING, notEmpty: true },
        username: { type: Sequelize.TEXT },
        about: { type: Sequelize.TEXT },
        email: { type: Sequelize.STRING, validate: { isEmail: {msg:"Email address must be valid!"} } },
        password: { type: Sequelize.STRING, allowNull: false },
        last_login: { type: Sequelize.DATE },
        createdAt: { type: Sequelize.DATE },
        updatedAt: { type: Sequelize.DATE },
        status: {
            type: Sequelize.ENUM('active', 'inactive'),
            defaultValue: 'active'
        }
    },
        {
            tableName: 'users',
            timestamps: true
        }
);

    return User;
};
