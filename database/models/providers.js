'use strict';
module.exports = function (sequelize, DataTypes) {
    let Provider = sequelize.define('Provider', {
        id: {
            type: DataTypes.BIGINT,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING(300),
            allowNull: false
        },
        url: {
            type: DataTypes.TEXT,
            allowNull: false
        }
    },
        {
            tableName: 'providers',
            timestamps: true
        }
    )


    Provider.getProviders = () => {
        return new Promise((resolve, reject) => {
            sequelize.query(`
        SELECT * FROM providers
      `, {
                    type: sequelize.QueryTypes.SELECT,
                    replacements: {
                    }
                }).then(result => {
                    resolve(result)
                }).catch(err => {
                    reject(err)
                })
        })
    }


    return Provider

}
