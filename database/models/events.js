module.exports = function (sequelize, DataTypes) {
  return sequelize.define('events', {
    id: {
      type: DataTypes.BIGINT,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: DataTypes.STRING,
      unique: true,
      allowNull: false
    },
  }, {
      timestamps: false,
      tableName: 'events'
    });
};
