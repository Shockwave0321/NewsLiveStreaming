// 'use strict';
// module.exports = (sequelize, DataTypes) => {
//     let Video = sequelize.define('Video', {
//         id: {
//           type: DataTypes.BIGINT,
//           primaryKey: true,
//           autoIncrement: true
//         },
//         providerId: {
//           type: DataTypes.BIGINT,
//         },
//         eventId: {
//           type: DataTypes.BIGINT,
//         },
//         title: {
//           type: DataTypes.STRING + ' CHARSET utf8 COLLATE utf8_unicode_ci',
//           allowNull: false
//         },
//         channel: {
//           type: DataTypes.STRING,
//           allowNull: false
//         },
//         url: {
//           type: DataTypes.STRING,
//           unique: true,
//           allowNull: false
//         },
//         status: {
//           type: DataTypes.BOOLEAN,
//           defaultValue: false
//         },
//         dateTime: {
//           type: DataTypes.STRING,
//         }
//       },
//          {
//           tableName : 'videos',
//           timestamps : true
//         }
//       );


//   Video.getVideos = () => {
//     return new Promise((resolve, reject) => {
//       sequelize.query(`
//         SELECT
//           p. NAME AS 'websitename',
//           p.url AS 'provider_url',
//           v.*
//         FROM
//           videos v
//         INNER JOIN providers p ON v.providerId = p.id
//       `, {
//         type: sequelize.QueryTypes.SELECT,
//         replacements: {
//         }
//       }).then(result => {
//         resolve(result)
//       }).catch(err => {
//         reject(err)
//       })
//     })
//   }

//   return Video;
// };


module.exports = function (sequelize, DataTypes) {
  return sequelize.define('videos', {
    id: {
      type: DataTypes.BIGINT,
      primaryKey: true,
      autoIncrement: true
    },
    providerId: {
      type: DataTypes.BIGINT,
    },
    eventId: {
      type: DataTypes.BIGINT,
    },
    title: {
      type: DataTypes.STRING,
      allowNull: false
    },
    channel: {
      type: DataTypes.STRING,
      allowNull: false
    },
    url: {
      type: DataTypes.STRING,
      unique: true,
      allowNull: false
    },
    status: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    dateTime: {
      type: DataTypes.STRING,
    }
  }, {
      timestamps: false,
      tableName: 'videos'
    });
};
