'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {

      return queryInterface.bulkInsert('providers', [
        {
          id: 1,
          name: 'Gak TV',
          url: 'http://www.gak-tv.com'
        },
        {
          id: 2,
          name: 'All TV DA',
          url: 'http://www.alltvda.com'
        },
        {
          id: 3,
          name: 'Popo TV',
          url: 'http://popotv.com'
        }
    ], {});
  
  },

  down: (queryInterface, Sequelize) => {
      return queryInterface.bulkDelete('providers', null, {});
  }
};
