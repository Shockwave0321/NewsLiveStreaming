const express = require('express')
const path = require('path')
const passport = require('passport');
const session = require('express-session');
const cookieParser = require('cookie-parser')
const bodyParser = require('body-parser')

const env = require('dotenv').load()
const settings = require('./config')
const db = require('./database/models')
const pages = require('./routes/pages')

var app = express()
const server = require('http').createServer(app)

// view engine setup
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'ejs')

// uncomment after placing your favicon in /public
// app.use(favicon(path.join(__dirname, 'public', 'webicon.ico')));
//app.use(logger('dev'));
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')))

app.use('/', pages)

// Passport
app.use(
    session({
        secret: 'rHUyjs6RmVOD06OdOTsVAyUUCxVXaWci',
        resave: true,
        saveUninitialized: true
    })
); // session secret
app.use(passport.initialize());
app.use(passport.session());

// Routes
const authRoute = require('./routes/auth.js')(app, passport);

// Load passport strategies
require('./config/passport/passport.js')(passport, db.user);

console.log('server listening at port :' + settings.server.port)

//module.exports = app;
server.listen(settings.server.port)
