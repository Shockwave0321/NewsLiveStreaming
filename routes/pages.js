const router = require('express').Router()
const db = require('../database/models')
const moment = require('moment')

//home page
router.get('/', async (req, res, err) => {
  let videos = await db.videos.findAll()
  res.render('pages/index', {
    videos: videos,
    moment: moment
  })

})



module.exports = router;
