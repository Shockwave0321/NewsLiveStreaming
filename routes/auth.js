module.exports = (app, passport) => {


    app.get('/signup', (req, res) => {
        res.render('pages/signup');
    });

    app.get('/signin', (req, res) => {
        res.render('pages/signin');
    });

    app.post(
        '/signup',
        passport.authenticate('local-signup', {
            successRedirect: '/home',
            failureRedirect: '/signup'
        })
    );

    app.get('/home', isLoggedIn, (req, res) => {
        res.render('pages/dashboard');
    });

    app.get('/logout', (req, res) => {
        req.session.destroy(err => {
            res.redirect('/signin');
        });
    });

    app.post(
        '/signin',
        passport.authenticate('local-signin', {
            successRedirect: '/home',
            failureRedirect: '/signin'
        })
    );

    function isLoggedIn(req, res, next) {
        if (req.isAuthenticated()) return next();

        res.redirect('/signin');
    }

};
