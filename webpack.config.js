const webpack = require('webpack');
const path = require('path');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const extractPlugin = new ExtractTextPlugin({
    filename: './assets/css/app.css'
})
const extractCSS = new ExtractTextPlugin('./assets/css/[name]-one.css');
const extractSASS = new ExtractTextPlugin('./assets/css/[name]-two.css');

const config = {

    "mode": process.env.NODE_ENV === "production" ? "production" : "development",
    context: path.resolve(__dirname, './'),
    entry: {
        app: './app.js'
    },
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: './sassets/js/[name].bundle.js'
    },
    devServer: {
        contentBase: path.resolve(__dirname, "./dist/assets/media"),
        stats: 'errors-only',
        open: true,
        port: 12000,
        compress: true
    },
    devtool: 'inline-source-map',
    module: {
        noParse: /jquery|lodash/,
        rules: [
            // babel-loader
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['env']
                    }
                }
            },
            // html-loader
            {
                test: /\.html$/,
                use: ['html-loader']
            },
            // scss-loader
            { 
                test: /\.css$/, 
                use: extractCSS.extract(['css-loader', 'style-loader']),
                sourceMap: true 
            },
            { 
                test: /\.scss$/,
                use: extractSASS.extract(['css-loader', 'sass-loader']), 
                    fallback: 'style-loader',
                sourceMap: true 
            },
            {
                test: /\.scss$/,
                include: [path.resolve(__dirname, 'src', 'assets', 'scss')],
                use: extractPlugin.extract({
                    use: ['css-loader', 'sass-loader'],
                    fallback: 'style-loader'
                })
            },
            //file-loader(for images)
            {
                test: /\.(jpg|png|gif|svg)$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: '[name].[ext]',
                            outputPath: './assets/media/'
                        }
                    }
                ]
            },
            //file-loader(for fonts)
            {
                test: /\.(woff|woff2|eot|ttf|otf)$/,
                use: ['file-loader']
            }
        ]
    },
    plugins: [
        new CleanWebpackPlugin(['dist']),
        new HtmlWebpackPlugin({
            template: 'index.html'
        }),
        extractPlugin,
        extractCSS,
        extractSASS
    ]
    
    
};


module.exports = config;