var each = require('each-series')
 
const eachSeries = (collection, iterator) => {
  return new Promise((resolve, reject) => {
    // iterator function(el, i, done)
    each(collection, iterator, function(err) {
      if (err) {
        reject(err)
      } else {
        resolve()
      }
    })
  })
}

module.exports = eachSeries