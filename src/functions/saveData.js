const db = require('../../database/models')
const eachSeries = require('./eachSeries')
const promise = require('./promise')

module.exports = async (videos, db) => {
  
  await eachSeries(videos, async (v, vIndex, done) => {
    // check if url is already in database...
    let { res: video, err: getVidErr } = await promise(db.videos.find({
      where: { url: v.url }
    }))

    console.log(v.url)
    
    if (video === null) {
      // create video

      // check if eventId exists...
      let eventId = null
      let { res: ev, err: getEventErr } = await promise(db.events.find({
        where: { name: v.event }
      }))

      if (ev) {
        eventId = ev.dataValues.id
      } else {
        let { res: createdEv, err: createEventErr } = await promise(db.events.create({
          name: v.event
        }))
        eventId = createdEv.dataValues.id
      }

      delete v.event
      v.eventId = eventId

      let { res: createdVid, err: createVidErr } = await promise(db.videos.create(v))
    } else {

      // video already exists... update status only
      video.update({
        status: v.status
      })
    }

    done()
  })

}