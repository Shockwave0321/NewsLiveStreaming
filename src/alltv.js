const Nightmare = require('nightmare')
const nightmare = Nightmare({ show: false })

let url = 'http://www.alltvda.com/broadcast_pc.php'
const saveData = require('./functions/saveData')
const db = require('../database/models')

nightmare.db = db

nightmare.on('console', (type, json) => {
  try {
    let data = JSON.parse(json)
    if (data.type === 'scraper') {
      let liveLinks = data.data
      liveLinks = liveLinks.map(l => {
        l.providerId = 2
        return l
      })
      saveData(liveLinks, nightmare.db)
    }
  } catch(err) {
    console.log('******error******', err)
  }
})

nightmare
  .goto(url)
  .wait(1000)
  .evaluate(() => {
    function output(obj){ 
      console.log(JSON.stringify(obj, null, 2))
    } 

    function getLink(rowEl) {

      const baseUrl = 'http://www.alltvda.com/'
      return baseUrl + rowEl.onclick.toString().split('\n')[1].trim().replace(`document.location.href='`, '').replace(`'`, '')
    }

    const INTERVAL = 5000

    setInterval(() => {
      try {
        let rowsEl = document.querySelectorAll('.chlist > tbody > tr[onclick]')
        let liveLinks = []
        rowsEl.forEach((rowEl, rowIndex) => {
          liveLinks.push({
            url: getLink(rowEl),
            channel: rowEl.children[0].innerText.trim(),
            event: rowEl.children[1].innerText.toLowerCase(),
            dateTime: rowEl.children[2].innerText.trim(),
            title: rowEl.children[3].innerText.trim(),
            status: rowEl.children[4].innerText.indexOf('air') === -1 ? true: false
          })
        })
        
        output({
          type: 'scraper',
          data: liveLinks
        })
      } catch(err) {

      }
    }, INTERVAL)
  })
  .then((liveLinks) => {
  })
  .catch(err => {
    console.log(err.message || err)
  })