const Nightmare = require('nightmare')
const nightmare = Nightmare({ show: false })

let url = 'http://www.gak-tv.com/player.php?bo_table=broadcast'
const saveData = require('./functions/saveData')
const db = require('../database/models')

nightmare.db = db

nightmare.on('console', (type, json, db) => {
  try {
    let data = JSON.parse(json)
    if (data.type === 'scraper') {
      let liveLinks = data.data
      liveLinks = liveLinks.map(l => {
        if (l.event === 'lol.png') {
          l.event = 'lol'
        }
        l.providerId = 1
        return l
      })
      // console.log(liveLinks)
      saveData(liveLinks, nightmare.db)
    }
  } catch(err) {
    console.log('******error******', err)
  }
}, db)

nightmare
  .goto(url)
  .wait(1000)
  .evaluate(() => {
    function output(obj){ 
      console.log(JSON.stringify(obj, null, 2))
    } 

    function getLink(rowEl) {

      const baseUrl = 'http://www.gak-tv.com'
      let firstPart = rowEl.onclick.toString().replace(`function onclick(event) {\n  location.href="..`, '')
      return baseUrl + firstPart.replace(`"\n}`, '')
    }

    const INTERVAL = 5000

    setInterval(() => {
      try {
        let rowsEl = document.querySelectorAll('tr.bg')
        let liveLinks = []
        rowsEl.forEach((rowEl, rowIndex) => {
          liveLinks.push({
            url: getLink(rowEl),
            channel: rowEl.children[0].innerText.trim(),
            event: rowEl.children[1].children[0].src.replace('http://www.gak-tv.com/player/', '').replace('_icon.png', ''),
            dateTime: rowEl.children[2].innerText.trim(),
            title: rowEl.children[3].innerText.trim(),
            status: rowEl.children[4].querySelector('img').src.indexOf('off') === -1 ? true: false
          })
        })
        
        output({
          type: 'scraper',
          data: liveLinks
        })
      } catch(err) {

      }
    }, INTERVAL)
  })
  .then((liveLinks) => {
  })
  .catch(err => {
    console.log(err.message || err)
  })